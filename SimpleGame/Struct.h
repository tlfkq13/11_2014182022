#pragma once

const int MAX_OBJECT{ 20 };
const int MAX_ENEMY{ 3 };

typedef struct POSITION {
	float xPos;
	float yPos;
	float zPos;
}Position;

typedef struct COLOR {
	float red;
	float green;
	float blue;
	float alpha;
}Color;

typedef struct VELOCITY {
	float xVel;
	float yVel;
	float zVel;
}Velocity;

typedef struct ACCELERATION {
	float xAcc;
	float yAcc;
	float zAcc;
}Acceleration;

typedef struct VOLUME {
	float xVol;
	float yVol;
	float zVol;
}Volume;

#define HERO_ID 0

#define GRAVITY 9.8f

#define WIDTH 500
#define HEIGHT 500

#define TYPE_NORMAL 0
#define TYPE_BULLET 1
#define TYPE_MONSTER 2
#define TYPE_WALL 3
#define TYPE_BOSSDOOR 4
#define TYPE_SPIDERMONSTER 5
#define TYPE_BOSS 6
