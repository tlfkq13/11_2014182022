#pragma once

#include "Struct.h"

enum DIRECTION {RIGHT,LEFT,IDLE};

class Object
{
public:
	Object();
	~Object();

public :
	// Init
	void InitPysics();

	//Set
	void SetPos(float xPos, float yPos, float zPos);
	void SetVel(float xVel, float yVel, float zVel);
	void SetVol(float xVol, float yVol, float zVol);
	void SetAcc(float xAcc, float yAcc, float zAcc);
	void SetMass(float mass);
	void SetColor(float red, float green, float blue, float alpha);
	void SetFrictCoef(float coef);
	void SetType(int type);
	void SetTextureID(int id);
	void SetParentObj(Object* obj);
	void SetIsDeath(bool d) { m_isDeath = d; }
	void SetDeathTime(float time);
	void SetDirection(int dir);
	void SetMoveDir(int dir);
	

	// Get
	Position GetPos() const { return m_pos; }
	Velocity GetVel() const { return m_vel; }
	Volume GetVol() const { return m_vol; }
	Acceleration GetAcc() const { return m_acc; }
	float GetMass() const { return m_mass; }
	Color GetColor() const { return m_color; }
	float GetFrictCoef() const { return m_frictCoef; }
	int GetType() const { return m_type; }
	int GetTextureID() const { return m_textureID; }
	Object* GetParentObj() const { return m_parent; }
	bool GetIsDeath() const { return m_isDeath; }
	int GetMoveDir()  const { return m_moveDir; }
	int GetDirection() const { return m_direction; }
	

	// update
	void Update(float elapsedTime);

	void AddForce(float x, float y, float z, float elapsedTime);

	bool CanShootBullet();
	void ResetShootBulletCoolTime();

	void Damage(float damage);
	void SetHP(float hp);
	float GetHP();
	float GetAge();
	float GetHPGauge();

	bool IsAncestor(Object* obj);

private:
	Position m_pos;
	Velocity m_vel;
	Volume m_vol;
	Acceleration m_acc;
	Color m_color;
	float m_mass;
	float m_frictCoef;
	float m_currhealthPoint;
	float m_fullhealthPoint;
	float m_age;
	int m_type;
	float m_deathTime;

	Object* m_parent = NULL;

	float m_remainingBulletCoolTime = 0.f;
	float m_defaultBulletCollTime = 0.2f;

	int m_textureID = -1;
	
	bool m_isDeath = false;

	int m_moveDir;
	int m_direction = RIGHT;
};