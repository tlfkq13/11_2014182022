#include "stdafx.h"
#include "Physics.h"

Physics::Physics()
{
}

Physics::~Physics()
{
}

// public
bool Physics::IsOverlap(Object* A, Object* B, int type)
{
	switch (type) {
	case 0:
		// BBOverlapTest
		return BBOverlapTest(A, B);
		break;
	case 1:
		break;
	case 2:
		break;
	}
}

void Physics::ProcessCollision(Object* A, Object* B)
{
	// A
	float aMass;
	float aVX, aVY, aVZ;
	aMass = A->GetMass();
	aVX = A->GetVel().xVel;
	aVY = A->GetVel().yVel;
	aVZ = A->GetVel().zVel;

	// B
	float bMass;
	float bVX, bVY, bVZ;
	bMass = B->GetMass();
	bVX = B->GetVel().xVel;
	bVY = B->GetVel().yVel;
	bVZ = B->GetVel().zVel;

	// final vel
	float afVX, afVY, afVZ;
	afVX = (aMass - bMass) / (aMass + bMass) * aVX
		+ (2.f * bMass) / (aMass + bMass) * bVX;
	afVY = (aMass - bMass) / (aMass + bMass) * aVY
		+ (2.f * bMass) / (aMass + bMass) * bVY;
	afVZ = (aMass - bMass) / (aMass + bMass) * aVZ
		+ (2.f * bMass) / (aMass + bMass) * bVZ;
	
	float bfVX, bfVY, bfVZ;
	bfVX = (2.f * aMass) / (aMass + bMass) * aVX
		+ (bMass - aMass) / (aMass + bMass) * bVX;
	bfVY = (2.f * aMass) / (aMass + bMass) * aVY
		+ (bMass - aMass) / (aMass + bMass) * bVY;
	bfVZ = (2.f * aMass) / (aMass + bMass) * aVZ
		+ (bMass - aMass) / (aMass + bMass) * bVZ;

	A->SetVel(afVX, afVY, afVZ);
	B->SetVel(bfVX, bfVY, bfVZ);
}

// private
bool Physics::BBOverlapTest(Object* A, Object* B)
{
	// Object A
	float aX, aY, aZ;
	float aMinX, aMinY, aMinZ;
	float aMaxX, aMaxY, aMaxZ;
	float aSizeX, aSizeY, aSizeZ;

	// Object B
	float bX, bY, bZ;
	float bMinX, bMinY, bMinZ;
	float bMaxX, bMaxY, bMaxZ;
	float bSizeX, bSizeY, bSizeZ;

	// calc box A
	aX = A->GetPos().xPos; aY = A->GetPos().yPos; aZ = A->GetPos().zPos;
	aSizeX = A->GetVol().xVol; aSizeY = A->GetVol().yVol; aSizeZ = A->GetVol().zVol;

	aMinX = aX - aSizeX / 2.f;	aMaxX = aX + aSizeX / 2.f;
	aMinY = aY - aSizeY / 2.f;	aMaxY = aY + aSizeY / 2.f;
	aMinZ = aZ - aSizeY / 2.f;	aMaxZ = aZ + aSizeZ / 2.f;

	// calc box A
	bSizeX = B->GetVol().xVol; bSizeY = B->GetVol().yVol; bSizeZ = B->GetVol().zVol;
	bX = B->GetPos().xPos; bY = B->GetPos().yPos; bZ = B->GetPos().zPos;

	bMinX = bX - bSizeX / 2.f;	bMaxX = bX + bSizeX / 2.f;
	bMinY = bY - bSizeY / 2.f;	bMaxY = bY + bSizeY / 2.f;
	bMinZ = bZ - bSizeY / 2.f;	bMaxZ = bZ + bSizeZ / 2.f;

	if (aMinX > bMaxX)
		return false;
	if (aMaxX < bMinX)
		return false;

	if (aMinY > bMaxY)
		return false;
	if (aMaxY < bMinY)
		return false;

	if (aMinZ > bMaxZ)
		return false;
	if (aMaxZ < bMinZ)
		return false;

	return true;
}