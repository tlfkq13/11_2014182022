#pragma once

#include "Renderer.h"
#include "Object.h"
#include "Struct.h"
#include "Physics.h"
#include "Sound.h"

class ScnMgr
{
public:
	ScnMgr();
	~ScnMgr();

public:
	int AddObject(float xPos, float yPos, float zPos, float xSize, float ySize, float zSize, float red, float green, float blue, float alpha, float xVel, float yVel, float zVel, float mass, float fricCoef, float hp, int type);
	void DeleteObject(int index);
	void KeyDownInput(unsigned char key, int x, int y);
	void KeyUpInput(unsigned char key, int x, int y);
	void SpecialKeyDownInput(unsigned char key, int x, int y);
	void SpecialKeyUpInput(unsigned char key, int x, int y);
	void Update(float elapsedTime);
	void RenderScene();
	void DrawGauge(int index, POSITION p, Volume v);

private:
	void DoGarbageCollection();

	

	Renderer* m_Renderer = NULL;
	Object* m_Object[MAX_OBJECT];
	Physics* m_Physics = NULL;
	Sound* m_Sound = NULL;

	// Key inputs
	bool m_keyW = false;
	bool m_keyA = false;
	bool m_keyS = false;
	bool m_keyD = false;
	bool m_keySP = false;

	// Special Key inputs
	bool m_keyUp = false;
	bool m_keyLeft = false;
	bool m_keyDown = false;
	bool m_keyRight = false;
};