#include "stdafx.h"
#include "ScnMgr.h"
#include <random>

int enemyAnimTexture = -1;
int enemySpiderTexture = -1;
int enemyBossTexture = -1;
int enemyTexture = -1;
int wallTexture = -1;
int doorTexture = -1;
int mapBG = -1;
int bossmapBG = -1;
int bulletTexture = -1;

int HeroAnimTexture = -1;
int g_testBG = -1;

int g_testParticle = -1;
int g_testParticleTex = -1;

int g_testSoundBG = -1;
int g_testSoundFire = -1;
int g_testSoundExplosion = -1;

int deathSound = -1;

ScnMgr::ScnMgr()
{
	m_Renderer = new Renderer(800, 600);

	if (!m_Renderer->IsInitialized()) {
		std::cout << "Renderer could not be initialized... \n";
	}
	
	// initialize physics
	m_Physics = new Physics();

	// initialize sound
	m_Sound = new Sound();

	//Init Object
	for (int i = 0; i < MAX_OBJECT; ++i)
		m_Object[i] = NULL;

	// Add Hero object
	m_Object[HERO_ID] = new Object;
	m_Object[HERO_ID]->SetColor(1.f, 1.f, 1.f, 1.f);
	m_Object[HERO_ID]->SetPos(0.f, 0.f, 0.f);
	m_Object[HERO_ID]->SetVol(1.f, 1.f, 1.f);
	m_Object[HERO_ID]->SetVel(0.f, 0.f, 0.f);
	m_Object[HERO_ID]->SetMass(1.f);
	m_Object[HERO_ID]->SetFrictCoef(0.7f);
	m_Object[HERO_ID]->SetHP(1000.0f);

	enemyAnimTexture = m_Renderer->GenPngTexture("./Textures/gfx/monsters/classic/monster_199_parabite_scarred.png");
	enemySpiderTexture=m_Renderer->GenPngTexture("./Textures/gfx/monsters/classic/monster_082_trite.png");
	
	HeroAnimTexture = m_Renderer->GenPngTexture("./Textures/gfx/characters/player2/000_baby_spider.png");
	m_Object[HERO_ID]->SetTextureID(HeroAnimTexture);
	wallTexture = m_Renderer->GenPngTexture("./Textures/gfx/maps/columns-7.png");
	mapBG = m_Renderer->GenPngTexture("./Textures/gfx/maps/map1.png");
	bossmapBG = m_Renderer->GenPngTexture("./Textures/gfx/backdrop/0a_library_nfloor.png");
	doorTexture = m_Renderer->GenPngTexture("./Textures/gfx/maps/bossdoor.png");

	//HeroAnimTexture = m_Renderer->GenPngTexture("./Textures/bg.png");
	//g_testBG = m_Renderer->GenPngTexture("./Textures/bg.png");
	g_testParticleTex = m_Renderer->GenPngTexture("./Textures/particle2.png");
	
	//enemyObject
	/*int temp = AddObject(
		2.f, 0.f, 0.f,
		1.0f,1.0f, 1.0f,
		1.f, 1.f, 1.f, 1.f,
		0.f, 0.f, 0.f,
		0.7f,
		0.7f,
		10.0f,
		ENEMY_MONSTER);
	m_Object[temp]->SetTextureID(HeroAnimTexture);*/

	//enemyObject
	float randX = rand() % 3;
	float randY = rand() % 3;
	std::cout << randX << endl;

	int enemy = AddObject(
		2.f, 0.f, 0.f,
		1.0f, 1.0f, 1.0f,
		1.f, 1.f, 1.f, 1.f,
		0.f, 0.f, 0.f,
		1.5f,
		1.5f,
		10.0f,
		TYPE_MONSTER);
	m_Object[enemy]->SetTextureID(enemyAnimTexture);

	enemy = AddObject(
		-2.f, 0.f, 0.f,
		1.0f, 1.0f, 1.0f,
		1.f, 1.f, 1.f, 1.f,
		0.f, 0.f, 0.f,
		1.5f,
		1.5f,
		10.0f,
		TYPE_MONSTER);
	m_Object[enemy]->SetTextureID(enemyAnimTexture);

	enemy = AddObject(
		-2.f, -1.7f, 0.f,
		1.0f, 1.0f, 1.0f,
		1.f, 1.f, 1.f, 1.f,
		0.f, 0.f, 0.f,
		1.5f,
		1.5f,
		10.0f,
		TYPE_SPIDERMONSTER);
	m_Object[enemy]->SetTextureID(enemySpiderTexture);


	int wall = AddObject(
		-3.8f, -3.f, 0.f,
		0.3f, 6.0f, 0.3f,
		1.f, 1.f, 1.f, 0.f,
		0.f, 0.f, 0.f,
		100000.f,
		1.f,
		1000.f,
		TYPE_WALL);
	m_Object[wall]->SetTextureID(wallTexture);

	wall = AddObject(
		3.8f, -3.f, 0.f,
		0.3f, 6.0f, 0.3f,
		1.f, 1.f, 1.f, 0.f,
		0.f, 0.f, 0.f,
		100000.f,
		1.f,
		1000.f,
		TYPE_WALL);
	m_Object[wall]->SetTextureID(wallTexture);
	
    int boosdoor = AddObject(
		0.f,2.0f,0.f,
		1.0f,1.0f,1.0f,
		1.f,1.f,1.f,1.f,
		0.f,0.f,0.f,
		1000000000000000000000.f,
		1.f,
		10.f,
		TYPE_BOSSDOOR);
	m_Object[boosdoor]->SetTextureID(doorTexture);

	int boss = AddObject(
		0.f, -1.8f, 0.f,
		1.5f, 1.5f, 1.5f,
		1.f, 1.f, 1.f, 1.f,
		0.f, 0.f, 0.f,
		10.f,
		10.f,
		10.f,
		TYPE_BOSS
	);
	enemyBossTexture = m_Renderer->GenPngTexture("./Textures/gfx/monsters/classic/monster_181_chubber.png");
	m_Object[boss]->SetTextureID(enemyBossTexture);

	g_testSoundBG = m_Sound->CreateBGSound("./Textures/gfx/maps/CSOnline.mp3");
	m_Sound->PlayBGSound(g_testSoundBG, true, 1.f);
	g_testSoundFire = m_Sound->CreateShortSound("./Sounds/fire.mp3");
	g_testSoundExplosion = m_Sound->CreateShortSound("./Sounds/explosion.mp3");
	// = m_Sound->CreateShortSound("./Sounds/shotgun.mp3");

	// particle
	g_testParticle = m_Renderer->CreateParticleObject(
		70,
		-0, -0,
		0, 0,
		15, 15,
		27, 27,
		-10, -10,
		5, 5
	);
}

ScnMgr::~ScnMgr()
{
	if (NULL != m_Renderer) {
		delete m_Renderer;
		m_Renderer = NULL;
	}
}

int ScnMgr::AddObject
	(float xPos, float yPos, float zPos, 
		float xSize, float ySize, float zSize, 
		float red, float green, float blue, float alpha, 
		float xVel, float yVel, float zVel, 
		float mass, 
		float fricCoef, 
		float hp,
		int type)
{
	// search empty slot
	int index = -1;

	for (int i = 0; i < MAX_OBJECT; ++i) {
		if (NULL == m_Object[i]) {
			index = i;
			break;
		}
	}
	if (-1 == index) {
		std::cout << "No more remaining object\n";
		return -1;
	}

	m_Object[index] = new Object;
	m_Object[index]->SetColor(red, green, blue, alpha);
	m_Object[index]->SetPos(xPos, yPos, zPos);
	m_Object[index]->SetVol(xSize, ySize, zSize);
	m_Object[index]->SetVel(xVel, yVel, zVel);
	m_Object[index]->SetMass(mass);
	m_Object[index]->SetFrictCoef(fricCoef);
	m_Object[index]->SetType(type);
	m_Object[index]->SetHP(hp);

	return index;
}
void ScnMgr::DeleteObject(int index)
{
	if (index < 0) {
		std::cout << "Negative index does not allowed. index:" << index << std::endl;
		return;
	}

	if (index >= MAX_OBJECT) {
		std::cout << "Requested index exceeds MAX_OBJECT. index:" << index << std::endl;
		return;
	}

	if (NULL != m_Object[index]) {
		delete m_Object[index];
		m_Object[index] = NULL;
	}
}
void ScnMgr::KeyDownInput(unsigned char key, int x, int y)
{
	if (key == 'w' || key == 'W') {
		m_keyW = true;
	}

	if (key == 'a' || key == 'A') {
		m_keyA = true;
	}	

	if (key == 's' || key == 'S') {
		m_keyS = true;
	}

	if (key == 'd' || key == 'D') {
		m_keyD = true;
	}

	if (key == ' ') {
		m_keySP = true;
	}
}
void ScnMgr::KeyUpInput(unsigned char key, int x, int y)
{
	if (key == 'w' || key == 'W') {
		m_keyW = false;
	}

	if (key == 'a' || key == 'A') {
		m_keyA = false;
	}

	if (key == 's' || key == 'S') {
		m_keyS = false;
	}

	if (key == 'd' || key == 'D') {
		m_keyD = false;
	}

	if (key == ' ') {
		m_keySP = false;
	}
}
void ScnMgr::SpecialKeyDownInput(unsigned char key, int x, int y)
{
	if (key == GLUT_KEY_UP) {
		m_keyUp = true;
	}

	if (key == GLUT_KEY_LEFT) {
		m_keyLeft = true;
	}

	if (key == GLUT_KEY_DOWN) {
		m_keyDown = true;
	}

	if (key == GLUT_KEY_RIGHT) {
		m_keyRight = true;
	}
}
void ScnMgr::SpecialKeyUpInput(unsigned char key, int x, int y)
{
	if (key == GLUT_KEY_UP) {
		m_keyUp = false;
	}

	if (key == GLUT_KEY_LEFT) {
		m_keyLeft = false;
	}

	if (key == GLUT_KEY_DOWN) {
		m_keyDown = false;
	}

	if (key == GLUT_KEY_RIGHT) {
		m_keyRight = false;
	}
}

void ScnMgr::Update(float elapsedTime)
{
	//std::cout << std::boolalpha 
	//	<< "w: " << m_keyW << ", a: " << m_keyA << ", s: " << m_keyS << ", d: " << m_keyD << std::endl;

	//std::cout << std::boolalpha
	//	<< "Up: " << m_keyUp << ", Left: " << m_keyLeft << ", Down: " << m_keyDown << ", Right: " << m_keyRight << std::endl;

	float fx, fy, fz;
	float fAmount = 10.f;
	fx = fy = fz = 0.f;

	if (m_keyW) {
		fy += 1.f;
	}
	if (m_keyS) {
		fy -= 1.f;
	}
	if (m_keyD) {
		fx += 1.f;
	}
	if (m_keyA) {
		fx -= 1.f;
	}
	if (m_keySP) {
		fz += 1.f;
	}

	float fSize = sqrt((fx*fx) + (fy*fy));
	if (fSize > FLT_EPSILON) {
		fx /= fSize;
		fy /= fSize;

		fx *= fAmount;
		fy *= fAmount;

		m_Object[HERO_ID]->AddForce(fx, fy, 0, elapsedTime);
	}
	if (fz > FLT_EPSILON) {
		POSITION p;
		p = m_Object[HERO_ID]->GetPos();
		if (p.zPos < FLT_EPSILON) {
			fz *= fAmount * 20.f;
			m_Object[HERO_ID]->AddForce(0, 0, fz, elapsedTime);
		}
	}

	float fxBullet, fyBullet, fzBullet;
	float vAmountBullet = 5.f;
	fxBullet = fyBullet = fzBullet = 0.f;

	if (m_keyUp) {
		fyBullet += 1.f;
	}
	if (m_keyDown) {
		fyBullet -= 1.f;
	}
	if (m_keyRight) {
		fxBullet += 1.f;
	}
	if (m_keyLeft) {
		fxBullet -= 1.f;
	}

	float fbSize = sqrtf((fxBullet*fxBullet) + (fyBullet*fyBullet) + (fzBullet*fzBullet));
	//
	//
	if (fbSize > FLT_EPSILON) {
		fxBullet /= fbSize;
		fyBullet /= fbSize;
		fzBullet /= fbSize;

		fxBullet *= vAmountBullet;
		fyBullet *= vAmountBullet;
		fzBullet *= vAmountBullet;

		// Add object for firing bullet
		Velocity HeroVel;
		HeroVel = m_Object[HERO_ID]->GetVel();
		HeroVel.xVel = HeroVel.xVel + fxBullet;
		HeroVel.yVel = HeroVel.yVel + fyBullet;
		HeroVel.zVel = 0.f;

		Position pos;
		pos = m_Object[HERO_ID]->GetPos();
		
		//BulletDefault
		float size = 0.3f;
		float mass = 0.7f;
		float fricCoef = 0.5f;
		Color c = { 1.f ,1.f, 1.f, 1.f };
		int type = TYPE_BULLET;

		
		
		// Check whether this obj can shoot bullet or not
		if (m_Object[HERO_ID]->CanShootBullet()) {
			int temp = AddObject(
				pos.xPos,pos.yPos,pos.zPos,
				size, size, size,
				1.f, 0.f, 0.f, 0.f,
				HeroVel.xVel, HeroVel.yVel, HeroVel.zVel,
				mass,
				fricCoef,
				1.0f,
				type);
			bulletTexture = m_Renderer->GenPngTexture("./Textures/gfx/maps/bullet.png");
			m_Object[temp]->SetParentObj(m_Object[HERO_ID]);
			m_Object[temp]->SetTextureID(bulletTexture);
			m_Object[HERO_ID]->ResetShootBulletCoolTime();
			// sound
			m_Sound->PlayShortSound(g_testSoundFire, false, 1.f);
		}

		
	}
	for (int src = 0; src < MAX_OBJECT; ++src)
	{
		for (int trg = src + 1; trg < MAX_OBJECT; ++trg)
		{
			if (NULL != m_Object[src] && NULL != m_Object[trg])
			{
				//Overlap Test
				if (m_Physics->IsOverlap(m_Object[src], m_Object[trg]))
				{
					// Collision processing
					std::cout << "Collision " << src << ", " << trg << std::endl;
					if (!m_Object[src]->IsAncestor(m_Object[trg]) &&
						!m_Object[trg]->IsAncestor(m_Object[src]))
					{
						if (m_Object[src]->GetType() != m_Object[trg]->GetType()) {

							if (true == m_Object[src]->GetIsDeath())continue;
							if (true == m_Object[trg]->GetIsDeath())continue;

							m_Physics->ProcessCollision(m_Object[src], m_Object[trg]);

							float srcHP, trgHP;
							srcHP = m_Object[src]->GetHP();
							trgHP = m_Object[trg]->GetHP();

							if (HERO_ID == src) {
								m_Object[src]->Damage(trgHP);
								continue;

							}
							if (TYPE_BULLET == m_Object[src]->GetType()) {
								m_Object[HERO_ID]->Damage(5.f);
								m_Object[src]->Damage(trgHP);
								continue;
							}
							else if (TYPE_BULLET == m_Object[trg]->GetType()) {
								m_Object[HERO_ID]->Damage(5.f);
								m_Object[trg]->Damage(srcHP);
							}
							else {
								m_Object[trg]->Damage(srcHP);
								m_Object[src]->Damage(trgHP);
								continue;
							}

							//m_Object[index]->SetColor(red, green, blue, alpha);
							m_Object[src]->SetColor(1.0f, 0.f, 0.f, 1.0f);

							//soundbj
							m_Sound->PlayShortSound(g_testSoundExplosion, false, 1.f);
						}
					}
				}
			}
		}
	}


	for (int i = 0; i < MAX_OBJECT; ++i) {
		if (NULL != m_Object[i])
			m_Object[i]->Update(elapsedTime);
	}
	
}

void ScnMgr::RenderScene()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearColor(0.0f, 0.3f, 0.3f, 1.0f);

	// background

	m_Renderer->DrawGround(0, 0, 0, 800, 600, 0, 1, 1, 1, 1, mapBG, 1.f);

	// particle
	// DrawParticle
	static float pTime = 0.f;
	pTime += 0.02;
	m_Renderer->DrawParticle(
		g_testParticle,
		0.f, 2.f, 0.f,
		1.f,
		1.f, 0.f, 0.f, 1.f,
		0.f, -50.f,
		1.f,
		pTime, 0.9f
	);


	// Renderer Test
	Position p{};
	Volume v{};
	Color c{};


	int texID = -1;

	for (int i = 0; i < MAX_OBJECT; ++i) {
		if (NULL != m_Object[i]) {
			p = m_Object[i]->GetPos();
			p.xPos = p.xPos * 100.f;
			p.yPos = p.yPos * 100.f;
			p.zPos = p.zPos * 100.f;

			v = m_Object[i]->GetVol();
			v.xVol = v.xVol * 100.f;
			v.yVol = v.yVol * 100.f;
			v.zVol = v.zVol * 100.f;
			c = m_Object[i]->GetColor();
			texID = m_Object[i]->GetTextureID();

			//m_Renderer->DrawSolidRect(p.xPos, p.yPos, p.zPos, v.xVol, c.red, c.green, c.blue, c.alpha);
			if (i == HERO_ID) {
				static int ttt = 0;
				static int delay = 0;
				m_Renderer->DrawTextureRectAnim(
					p.xPos, p.yPos, p.zPos,
					v.xVol, v.yVol, v.zVol,
					c.red, c.green, c.blue, c.alpha,
					texID,
					8, 1,
					ttt, 0
				);

				ttt++;
				ttt = ttt % 8;

				DrawGauge(i, p, v);
			}
			

		//Bullet_Anim
		else if (m_Object[i]->GetType() == TYPE_BULLET)
		{
			m_Renderer->DrawTextureRectAnim(
				p.xPos, p.yPos, p.zPos,
				v.xVol, v.yVol, v.zVol,
				c.red, c.green, c.blue, c.alpha,
				texID,
				8, 4,
				2, 3
			);
			m_Renderer->DrawParticle(
				g_testParticle,
				p.xPos, p.yPos, p.zPos,
				1,
				1, 1, 1, 1,
				p.xPos - 20.f, 0.f,
				g_testParticleTex,
				1.f,
				m_Object[i]->GetAge(), 0.5f
			);
		}
		//EnemyMonster_Anim
		else if (m_Object[i]->GetType() == TYPE_MONSTER)
		{
			if (true == m_Object[i]->GetIsDeath())continue;
			if (m_Object[i]->GetDirection() == LEFT) {
				static int ttt = 0;
				static int delay = 0;
				m_Renderer->DrawTextureRectAnim(
					p.xPos, p.yPos, p.zPos,
					v.xVol, v.yVol, v.zVol,
					c.red, c.green, c.blue, c.alpha,
					texID,
					3, 3,
					ttt, 0
				);
				if (delay == 10) {
					ttt++;
					ttt = ttt % 3;
					delay = 0;
				}
				delay++;
			}
			else if (m_Object[i]->GetDirection() == RIGHT) {
				static int ttt = 0;
				static int delay = 0;
				m_Renderer->DrawTextureRectAnim(
					p.xPos, p.yPos, p.zPos,
					v.xVol, v.yVol, v.zVol,
					c.red, c.green, c.blue, c.alpha,
					texID,
					3, 3,
					ttt, 0
				);
				if (delay == 10) {
					ttt++;
					ttt = ttt % 3;
					delay = 0;
				}
				delay++;
			}
			else {
				static int ttt = 0;
				static int delay = 0;
				m_Renderer->DrawTextureRectAnim(
					p.xPos, p.yPos, p.zPos,
					v.xVol, v.yVol, v.zVol,
					c.red, c.green, c.blue, c.alpha,
					texID,
					3, 3,
					ttt, 0
				);
				if (delay == 10) {
					ttt++;
					ttt = ttt % 3;
					delay = 0;
				}
				delay++;
			}
		}
		else if (m_Object[i]->GetType() == TYPE_BOSS) {
			static int ttt = 0;
			static int delay = 0;
			m_Renderer->DrawTextureRectAnim(
				p.xPos, p.yPos, p.zPos,
				v.xVol, v.yVol, v.zVol,
				c.red, c.green, c.blue, c.alpha,
				texID,
				3, 3,
				0, ttt
			);
			if (delay == 10) {
				ttt++;
				ttt = ttt % 3;
				delay = 0;
			}
			delay++;
			DrawGauge(i, p, v);
		}
		else if (m_Object[i]->GetType() == TYPE_SPIDERMONSTER)
		{
			static int ttt = 0;
			static int delay = 0;
			m_Renderer->DrawTextureRectAnim(
				p.xPos, p.yPos, p.zPos,
				v.xVol, v.yVol, v.zVol,
				c.red, c.green, c.blue, c.alpha,
				texID,
				4, 3,
				2, ttt
			);
			if (delay == 10) {
				ttt++;
				ttt = ttt % 4;
				delay = 0;
			}
			delay++;

		}

		else {
			m_Renderer->DrawTextureRect(
				p.xPos, p.yPos, p.zPos,
				v.xVol, v.yVol, v.zVol,
				c.red, c.green, c.blue, c.alpha,
				texID);


		}

	}
	for (int i = 0; i < MAX_OBJECT; ++i) {
		static int delay = 0;
		if (NULL != m_Object[i]) {
			if (delay == 30) {
				m_Object[i]->SetColor(1.f, 1.f, 1.f, 1.f);
				delay = 0;
			}
			delay++;
		}
	}

	DoGarbageCollection();
}
}

void ScnMgr::DoGarbageCollection()
{
	// Delete bullets those zero vel
	for (int i = 0; i < MAX_OBJECT; ++i) {
		if (m_Object[i] != NULL) {
			if (m_Object[i]->GetType() == TYPE_BULLET) {
				Velocity v;
				v = m_Object[i]->GetVel();
				float vSize = sqrtf(v.xVel*v.xVel + v.yVel*v.yVel + v.zVel*v.zVel);
				if (vSize < FLT_EPSILON) {
					DeleteObject(i);
					continue;
				}
			}
			float hp;
			hp = m_Object[i]->GetHP();
			if (hp < FLT_EPSILON) {
				if (m_Object[i]->GetType() == TYPE_BULLET){
					DeleteObject(i);
				}
				else {
					if (true == m_Object[i]->GetIsDeath())continue;
					float now = m_Object[i]->GetAge();

					DeleteObject(i);
				}
				continue;

			}
		}
	}
}

void ScnMgr::DrawGauge(int index, POSITION p, VOLUME v)
{
	m_Renderer->DrawSolidRectGauge(
		p.xPos, p.yPos, p.zPos,
		0.f, v.yVol / 2.f + 5.f, 0.f,
		v.xVol, 10.f, 1.f,
		1.f, 0.f, 0.f, 1.f,
		m_Object[index]->GetHPGauge() * 100.f);
}