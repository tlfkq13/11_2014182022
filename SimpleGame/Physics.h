#pragma once
#include <iostream>
#include "Object.h"

class Physics
{
public:
	Physics();
	~Physics();

public:
	bool IsOverlap(Object* A, Object* B, int type = 0);
	void ProcessCollision(Object* A, Object* B);

private:
	bool BBOverlapTest(Object* A, Object* B);
};